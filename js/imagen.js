var inputImagen = document.getElementById("inputImagen");
var seleccionarBoton = document.getElementById("seleccionarBoton");
var nombreArchivo = document.getElementById("nombreArchivo");
var imagenMostrada = document.getElementById("imagenMostrada");

seleccionarBoton.addEventListener("click", function() {
  inputImagen.click();
});

inputImagen.addEventListener("change", function() {
  if (inputImagen.files && inputImagen.files[0]) {
    var archivo = inputImagen.files[0];
    
    nombreArchivo.textContent = "Archivo seleccionado: " + archivo.name;
    nombreArchivo.style.display = "block";

    var reader = new FileReader();

    reader.onload = function(e) {
      imagenMostrada.src = e.target.result;
      imagenMostrada.style.display = "block";
    };
    reader.readAsDataURL(archivo);
  }
});