let paises = [
    {
        "pais": "Estados Unidos",
        "moneda": "Dólar estadounidense",
        "valor_cambio": 1.00
    },
    {
        "pais": "España",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Japón",
        "moneda": "Yen",
        "valor_cambio": 110.62
    },
    {
        "pais": "Reino Unido",
        "moneda": "Libra esterlina",
        "valor_cambio": 0.74
    },
    {
        "pais": "Canadá",
        "moneda": "Dólar canadiense",
        "valor_cambio": 1.28
    },
    {
        "pais": "Australia",
        "moneda": "Dólar australiano",
        "valor_cambio": 1.36
    },
    {
        "pais": "China",
        "moneda": "Yuan",
        "valor_cambio": 6.37
    },
    {
        "pais": "India",
        "moneda": "Rupia india",
        "valor_cambio": 74.92
    },
    {
        "pais": "Brasil",
        "moneda": "Real brasileño",
        "valor_cambio": 5.35
    },
    {
        "pais": "México",
        "moneda": "Peso mexicano",
        "valor_cambio": 20.17
    },
    {
        "pais": "Alemania",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Francia",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Italia",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Rusia",
        "moneda": "Rublo ruso",
        "valor_cambio": 80.31
    },
    {
        "pais": "Sudáfrica",
        "moneda": "Rand",
        "valor_cambio": 14.88
    }
]


const slider = document.getElementById('slider');
slider.addEventListener('click',function(){
    let value = slider.value;
    console.log(value);
    if(value == 1){
        console.log(paises[0].pais);
        document.getElementById('pais').value = paises[0].pais;
        document.getElementById('moneda').value = paises[0].moneda;
        document.getElementById('valor').value = paises[0].valor_cambio;
    }
    if(value == 2){
        console.log(paises[1].pais);
        document.getElementById('pais').value = paises[1].pais;
        document.getElementById('moneda').value = paises[1].moneda;
        document.getElementById('valor').value = paises[1].valor_cambio;
    }
    if(value == 3){
        console.log(paises[2].pais);
        document.getElementById('pais').value = paises[2].pais;
        document.getElementById('moneda').value = paises[2].moneda;
        document.getElementById('valor').value = paises[2].valor_cambio;
    }
    if(value == 4){
        console.log(paises[3].pais);
        document.getElementById('pais').value = paises[3].pais;
        document.getElementById('moneda').value = paises[3].moneda;
        document.getElementById('valor').value = paises[3].valor_cambio;
    }
    if(value == 5){
        console.log(paises[4].pais);
        document.getElementById('pais').value = paises[4].pais;
        document.getElementById('moneda').value = paises[4].moneda;
        document.getElementById('valor').value = paises[4].valor_cambio;
    }
    if(value == 6){
        console.log(paises[5].pais);
        document.getElementById('pais').value = paises[5].pais;
        document.getElementById('moneda').value = paises[5].moneda;
        document.getElementById('valor').value = paises[5].valor_cambio;
    }
    if(value == 7){
        console.log(paises[6].pais);
        document.getElementById('pais').value = paises[6].pais;
        document.getElementById('moneda').value = paises[6].moneda;
        document.getElementById('valor').value = paises[6].valor_cambio;
    }
    if(value == 8){
        console.log(paises[7].pais);
        document.getElementById('pais').value = paises[7].pais;
        document.getElementById('moneda').value = paises[7].moneda;
        document.getElementById('valor').value = paises[7].valor_cambio;
    }
    if(value == 9){
        console.log(paises[8].pais);
        document.getElementById('pais').value = paises[8].pais;
        document.getElementById('moneda').value = paises[8].moneda;
        document.getElementById('valor').value = paises[8].valor_cambio;
    }
    if(value == 10){
        console.log(paises[9].pais);
        document.getElementById('pais').value = paises[9].pais;
        document.getElementById('moneda').value = paises[9].moneda;
        document.getElementById('valor').value = paises[9].valor_cambio;
    }
    if(value == 11){
        console.log(paises[10].pais);
        document.getElementById('pais').value = paises[10].pais;
        document.getElementById('moneda').value = paises[10].moneda;
        document.getElementById('valor').value = paises[10].valor_cambio;
    }
    if(value == 12){
        console.log(paises[11].pais);
        document.getElementById('pais').value = paises[11].pais;
        document.getElementById('moneda').value = paises[11].moneda;
        document.getElementById('valor').value = paises[11].valor_cambio;
    }
    if(value == 13){
        console.log(paises[12].pais);
        document.getElementById('pais').value = paises[12].pais;
        document.getElementById('moneda').value = paises[12].moneda;
        document.getElementById('valor').value = paises[12].valor_cambio;
    }
})